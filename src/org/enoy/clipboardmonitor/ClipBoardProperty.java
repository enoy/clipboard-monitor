package org.enoy.clipboardmonitor;

import java.util.Random;

import org.enoy.clipboardmonitor.ClipBoardProperty.ClipBoardContent;

import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.paint.Color;

public class ClipBoardProperty extends SimpleObjectProperty<ClipBoardContent>
{

	private static final long DEFAULT_SLEEP = 150;

	private Clipboard clipboard;
	private boolean stopped;
	private Thread updateThread;
	private long refreshRate;

	public ClipBoardProperty(long refreshRate)
	{
		super(new ClipBoardContent(null, ""));
		this.clipboard = Clipboard.getSystemClipboard();
		this.refreshRate = refreshRate;
		startListening();
	}

	public ClipBoardProperty()
	{
		this(DEFAULT_SLEEP);
	}

	private void updateClipboardLoop()
	{
		while (!stopped)
		{
			// TODO: image file? file?...
			Platform.runLater(() -> {
				Object data = get().getData();
				if (clipboard.getImage() != null)
				{
					Image image = clipboard.getImage();
					if (data instanceof Image)
					{
						Image current = (Image) data;
						if (!imagesEqualsTest(current, image, 1000))
						{
							set(new ClipBoardContent(DataFormat.IMAGE, clipboard.getImage()));
						}
					}
					else
					{
						set(new ClipBoardContent(DataFormat.IMAGE, clipboard.getImage()));
					}
				}
				else if (clipboard.getFiles() != null && clipboard.getFiles().size() > 0)
				{
					if (!data.equals(clipboard.getFiles()))
					{
						set(new ClipBoardContent(DataFormat.FILES, clipboard.getFiles()));
					}
				}
				else if (clipboard.getHtml() != null)
				{
					String html = clipboard.getHtml();
					if (!data.equals(html))
					{
						set(new ClipBoardContent(DataFormat.HTML, html));
					}
				}
				else if (clipboard.getString() != null)
				{
					String clipBoardString = clipboard.getString();/* .replaceAll("\n|\r", " ").trim() */
					if (!data.equals(clipBoardString))
					{
						set(new ClipBoardContent(DataFormat.PLAIN_TEXT, clipBoardString));
					}
				}
			});
			sleep();
		}
	}

	private boolean imagesEqualsTest(Image img1, Image img2, long checks)
	{
		if (img1.getWidth() == img2.getWidth() && img1.getHeight() == img2.getHeight())
		{
			Random r = new Random();
			for (long i = 0; i < checks; i++)
			{
				int x = r.nextInt((int) img1.getWidth());
				int y = r.nextInt((int) img1.getHeight());
				Color pixel1 = img1.getPixelReader().getColor(x, y);
				Color pixel2 = img2.getPixelReader().getColor(x, y);
				if (pixel1.equals(pixel2))
					continue;
				return false;
			}
			return true;
		}
		return false;
	}

	private void sleep()
	{
		try
		{
			Thread.sleep(refreshRate);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void stopListening()
	{
		stopped = true;
	}

	public synchronized void startListening()
	{
		stopped = false;
		if (updateThread == null)
		{
			updateThread = new Thread(this::updateClipboardLoop);
			updateThread.start();
		}
		else if (!updateThread.isAlive())
		{
			updateThread.start();
		}
	}

	public static class ClipBoardContent
	{
		private DataFormat format;
		private Object data;

		public ClipBoardContent(DataFormat format, Object data)
		{
			super();
			this.format = format;
			this.data = data;
		}

		public DataFormat getFormat()
		{
			return format;
		}

		public void setFormat(DataFormat format)
		{
			this.format = format;
		}

		public Object getData()
		{
			return data == null ? "" : data;
		}

		public void setData(Object data)
		{
			this.data = data;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj == this)
				return true;
			if (obj instanceof ClipBoardContent)
			{
				ClipBoardContent other = (ClipBoardContent) obj;
				return this.getFormat().equals(other.getFormat()) //
					&& this.getData().equals(other.getData());
			}
			else
				return false;
		}

	}

}
