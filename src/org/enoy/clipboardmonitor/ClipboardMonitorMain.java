package org.enoy.clipboardmonitor;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ClipboardMonitorMain extends Application
{

	private ClipBoardProperty clipBoardProperty;

	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{

		clipBoardProperty = new ClipBoardProperty(500);
		Scene scene = new Scene(new ClipboardMonitor(getHostServices(), clipBoardProperty));
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

		primaryStage.initStyle(StageStyle.UTILITY);
		primaryStage.setTitle("Clipboard Monitor");
		primaryStage.setScene(scene);
		primaryStage.setOnHiding(e -> clipBoardProperty.stopListening());
		// primaryStage.setResizable(false);
		primaryStage.setAlwaysOnTop(true);
		primaryStage.show();
		primaryStage.setX(primaryScreenBounds.getWidth() - primaryStage.getWidth());
		primaryStage.setY(primaryScreenBounds.getHeight() - primaryStage.getHeight());

	}

}
