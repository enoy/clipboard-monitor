package org.enoy.clipboardmonitor;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class ClipboardMonitor extends StackPane
{

	@FXML
	private HBox hBoxHyperLink;
	@FXML
	private TextArea textArea;
	@FXML
	private ImageView imageView;
	@FXML
	private ListView<File> listView;
	@FXML
	private Hyperlink hyperLink;
	@FXML
	private WebView webView;

	private HostServices hostServices;

	private double mouseX;
	private double mouseY;
	private double windowX;
	private double windowY;

	@SuppressWarnings("unchecked")
	public ClipboardMonitor(HostServices hostServices, ClipBoardProperty clipBoardProperty) throws IOException
	{
		String url = ClipboardMonitor.class.getResource("/fxml/clipboardmonitor.fxml").toExternalForm();
		FXMLLoader loader = new FXMLLoader(new URL(url));
		loader.setRoot(this);
		loader.setController(this);
		// loader.load(ClipboardMonitor.class.getResourceAsStream("/fxml/clipboardmonitor.fxml"));
		loader.load();

		this.hostServices = hostServices;

		clipBoardProperty.addListener((v, o, n) -> {

			DataFormat format = n.getFormat();
			Object data = n.getData();

			if (format == DataFormat.PLAIN_TEXT)
			{
				// Text
				String text = (String) data;
				String textTrim = text.trim();
				URL textUrl = null;

				// Check if url
				try
				{
					textUrl = new URL(textTrim);
				}
				catch (MalformedURLException e1)
				{
					// Ignore
				}

				if (textUrl != null)
				{
					hyperLink.setText(textTrim);
					hBoxHyperLink.setVisible(true);
				}
				else
				{
					textArea.setText((String) data);
					textArea.setVisible(true);
				}
			}
			else if (format == DataFormat.FILES)
			{
				// File list
				listView.getItems().setAll((List<File>) data);
				listView.setVisible(true);
			}
			else if (format == DataFormat.IMAGE)
			{
				// Image
				imageView.setImage((Image) data);
				imageView.setVisible(true);
			}
			else if (format == DataFormat.HTML)
			{
				webView.getEngine().loadContent((String) data);
				webView.setVisible(true);
			}
			else
			{
				// No content/not supported
				// everything invisible
				textArea.setVisible(true);
				textArea.setVisible(false);
			}

		});

		setupVisibilities(hBoxHyperLink, textArea, listView, imageView, webView);

		listView.setOnMouseClicked(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2)
			{
				File selectedFile = listView.getSelectionModel().getSelectedItem();
				if (selectedFile != null && selectedFile.exists())
				{
					hostServices.showDocument(selectedFile.toURI().toString());
				}
			}
			e.consume();
		});

		Callback<ListView<File>, ListCell<File>> callbackListCellFactory =
			TextFieldListCell.forListView(new StringConverter<File>()
			{
				@Override
				public String toString(File object)
				{
					return object.getName() + " - [" + object.getAbsolutePath() + "]";
				}

				@Override
				public File fromString(String string)
				{
					return null;
				}
			});

		listView.setCellFactory(listView -> {
			ListCell<File> fileCell = callbackListCellFactory.call(listView);
			Platform.runLater(() -> addContextMenuToFileCell(fileCell));
			return fileCell;
		});

		this.setOnMousePressed(e -> {
			mouseX = e.getScreenX();
			mouseY = e.getScreenY();
			windowX = getWindow().getX();
			windowY = getWindow().getY();
		});

		this.setOnMouseDragged(e -> {
			double x = e.getScreenX() - mouseX + windowX;
			double y = e.getScreenY() - mouseY + windowY;
			getWindow().setX(x);
			getWindow().setY(y);
		});

		imageView.fitWidthProperty().bind(this.widthProperty());
		imageView.fitHeightProperty().bind(this.heightProperty());
	}

	public void setupVisibilities(final Node... nodes)
	{

		for (int i = 0; i < nodes.length; i++)
		{
			final Node node = nodes[i];
			node.visibleProperty().addListener((v, o, n) -> {
				if (n)
				{
					for (int j = 0; j < nodes.length; j++)
					{
						Node a = nodes[j];
						if (!a.equals(node))
							a.setVisible(false);
					}
				}
			});
		}

	}

	private void addContextMenuToFileCell(ListCell<File> fileCell)
	{
		MenuItem showInFolder = new MenuItem("Show in System Explorer");
		showInFolder.setUserData(fileCell.getItem());
		showInFolder.setOnAction(this::showInFolder);
		ContextMenu cm = new ContextMenu(showInFolder);
		fileCell.setContextMenu(cm);
	}

	private void showInFolder(ActionEvent e)
	{
		if (e.getSource() instanceof MenuItem)
		{
			MenuItem menuItem = (MenuItem) e.getSource();
			File file = (File) menuItem.getUserData();
			file = file.getParentFile();
			if (file != null)
			{
				hostServices.showDocument(file.toURI().toString());
			}
		}
	}

	private Window getWindow()
	{
		return getScene().getWindow();
	}

	@FXML
	void openLink()
	{
		hostServices.showDocument(hyperLink.getText());
	}

	@FXML
	void quit()
	{
		Platform.exit();
	}

}
